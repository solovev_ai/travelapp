package ru.edu.error;

public class Errors {

    public static final String SOME_ERROR_CODE = "SOME_ERROR_CODE";
    public static final String UPDATE_ERROR = "FAILED_TO_UPDATE";
    public static final String CITY_NOT_FOUND = "CITY_NOT_FOUND";
    public static final String ARGUMENT_ERROR = "ARGUMENT_ERROR";
    public static final String CITY_ALREADY_EXISTS = "CITY_ALREADY_EXISTS";
    public static final String SQL_EXCEPTION = "SQL_EXCEPTION";
    public static final String DELETE_ERROR = "FAILED_TO_DELETE";
    public static final String DELETECITYSERVICE_ERROR = "FAILED_DELETECITY_IN_CITYSERVICE";
    public static final String GETCITY_ERROR = "FAILED_GETSITY_IN_CITYSERVICE";
    public static final String GETDISTANCE_ERROR = "FAILED_GETDISTANCE_IN_CITYSERVICE";
    public static final String DELETECITYREST_ERROR = "FAILED_TO_DELETE_IN_CITYRESTCONTROLLER";
    public static final String WEATHERCITYSERVICE_ERROR = "FAILED_GETWEATHER_IN_CITYSERVICE";
}
