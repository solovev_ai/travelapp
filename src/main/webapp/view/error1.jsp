<%@ page isELIgnored="false" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/static/css/style.css"/>
    <title>Ошибка</title>
</head>
<body>
<main>
    <div class="content__holder">
        <h1>Ошибка</h1>
        <div>
            ${error}
        </div>
    </div>
</main>
</body>
</html>